﻿class WriteAndReadFile
{
    static string FILE_PATH = @"D:\Study process\C# basic study\C#_Basic_Homework_Slide_10_Directory-And-File\C#_Basic_Homework_Slide_10_Directory-And-File\GuessNumber\History.txt";

    public bool isFileExist(string filePath)
    {
        if (File.Exists(filePath))
        {
            return true;
        }
        return false;
    }
    public void WriteHistory()
    {
        string history = FILE_PATH;
        string content = "";
        if(isFileExist(history))
        {
            content = "Score: " +GuessNumber.countGuess;
            File.WriteAllText(history, content);
            Console.WriteLine("History save success !");
        }
    }

    public void ReadHistory()
    {
        string filePath = FILE_PATH;

        if (isFileExist(filePath) == true)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }
        }
        else
        {
            throw new Exception("Can not read history !");
        }
    }
}