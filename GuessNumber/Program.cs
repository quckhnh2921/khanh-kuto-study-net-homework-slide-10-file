﻿GuessNumber game = new GuessNumber();
WriteAndReadFile writeAndReadFile = new WriteAndReadFile();
int choice = 0;
do
{
    try
    {
        game.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                game.GuessRandomNumber();
                writeAndReadFile.WriteHistory();
                break;
            case 2:
                writeAndReadFile.ReadHistory();
                break;
            default:
                Console.WriteLine("BYE BYE !");
                break;
        }
    }catch(Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
    
} while (choice < 3);