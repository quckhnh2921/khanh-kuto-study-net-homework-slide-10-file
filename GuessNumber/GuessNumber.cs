﻿class GuessNumber
{
    public static int countGuess = 0;
    public int RandomNumber()
    {
        int randomNumber = Random.Shared.Next(1, 100);
        return randomNumber;
    }

    public void GuessRandomNumber()
    {
        var randomNumber = RandomNumber();
        int guessNumber = 0;
        do
        {
            Console.Write("Enter your number: ");
            guessNumber = int.Parse(Console.ReadLine());
            if(guessNumber < 0)
            {
                throw new Exception("Guess number is out of range from 1 to 100");
            }
            if (guessNumber < randomNumber)
            {
                Console.WriteLine("Your number is smaller than random number ! Try higher");
                countGuess++;
            }
            else if (guessNumber > randomNumber)
            {
                Console.WriteLine("Your number is higher than randon number ! Try smaller");
                countGuess++;
            }
            else
            {
                Console.WriteLine("Correct ! Random number is: " + randomNumber);
                countGuess++;
                Console.WriteLine("Your score: "+countGuess);
            }
        } while (randomNumber != guessNumber);
    }
    public void MainMenu()
    {
        Console.WriteLine("------GUESS-NUMBER------");
        Console.WriteLine("|     1. Play game     |");
        Console.WriteLine("|     2. View history  |");
        Console.WriteLine("|     3. Exit          |");
        Console.WriteLine("------------------------");
    }
}