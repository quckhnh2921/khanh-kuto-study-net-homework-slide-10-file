﻿class RockScissorPaper
{

    #region Properties
    const int KEO = 1;
    const int BUA = 2;
    const int BAO = 3;
    public static int DRAW = 0;
    public static int PLAYER_1_WIN = 1;
    public static int PLAYER_2_WIN = 2;
    public static int bestScore = 0;
    public static int countPlayer1Win = 0;
    public static int countPlayer2Win = 0;
    #endregion
    #region Game Menu
    public void MainMenu()
    {
        Console.WriteLine("------MAIN-MENU------");
        Console.WriteLine("|  1. Play Game     |");
        Console.WriteLine("|  2 .View history  |");
        Console.WriteLine("|  3. Exit          |");
        Console.WriteLine("---------------------");
    }
    public void GameMenu()
    {
        Console.WriteLine("------ROCK-SCISSOR-PAPER------");
        Console.WriteLine("|         1: KEO             |");
        Console.WriteLine("|         2: BUA             |");
        Console.WriteLine("|         3: BAO             |");
        Console.WriteLine("------------------------------");
    }
    #endregion
    #region Game Rule
    public void GraphicDisplay(int player1Choose, int player2Choose)
    {
        if (player1Choose == KEO)
        {
            Console.Write("✂️");
        }
        else if (player1Choose == BUA)
        {
            Console.Write("🔨");
        }
        else if (player1Choose == BAO)
        {
            Console.Write("🕸️");
        }
        Console.Write("💥");
        if (player2Choose == KEO)
        {
            Console.WriteLine("✂️");
        }
        else if (player2Choose == BUA)
        {
            Console.WriteLine("🔨");
        }
        else if (player2Choose == BAO)
        {
            Console.WriteLine("🕸️");
        }
    }
    public int GameRule(int player1Choose, int player2Choose)
    {
        bool player1Win = player1Choose == KEO && player2Choose == BAO 
            || player1Choose == BUA && player2Choose == KEO 
            || player1Choose == BAO && player2Choose == BUA;
        bool player2Win = player2Choose == KEO && player1Choose == BAO 
            || player2Choose == BUA && player1Choose == KEO 
            || player2Choose == BAO && player1Choose == BUA;
        GraphicDisplay(player1Choose, player2Choose);
        if (player1Choose < 1 || player1Choose > 3 || player2Choose < 1 || player2Choose > 3)
        {
            throw new ArgumentException("Player choose is out of range from 1 to 3");
        }
        if (player1Win)
        {
            return PLAYER_1_WIN;
        }
        if (player2Win)
        {
            return PLAYER_2_WIN;
        }
        return DRAW;
    }
    #endregion
    #region Game Result
    public string GameResult(int result)
    {
        if (result == PLAYER_1_WIN)
        {
            countPlayer1Win++;
            return "Player 1 Win !";
        }
        if (result == PLAYER_2_WIN)
        {
            countPlayer2Win++;
            return "Player 2 Win !";
        }
        return "Draw !";
    }

    public static int BestScore()
    {
        if (countPlayer1Win > countPlayer2Win)
        {
            return bestScore = countPlayer1Win;
        }
        if (countPlayer2Win > countPlayer1Win)
        {
            return bestScore = countPlayer2Win;
        }
        return bestScore;
    }
    #endregion
}