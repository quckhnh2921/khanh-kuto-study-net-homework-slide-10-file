﻿class WriteAndReadFile
{
    static string Player1History = @"D:\Study process\C# basic study\C#_Basic_Homework_Slide_10_Directory-And-File\C#_Basic_Homework_Slide_10_Directory-And-File\RockScissorPaperHistory\Player1History.txt";
    static string Player2History = @"D:\Study process\C# basic study\C#_Basic_Homework_Slide_10_Directory-And-File\C#_Basic_Homework_Slide_10_Directory-And-File\RockScissorPaperHistory\Player2History.txt";

    public void HistoryMenu()
    {
        Console.WriteLine("-----HISTORY-MENU-----");
        Console.WriteLine("|   1. Player 1      |");
        Console.WriteLine("|   2. Player 2      |");
        Console.WriteLine("----------------------");
    }
    public void WriteGameHistory(int result)
    {
        string player1History = Player1History;
        string player2History = Player2History;
        string content = "";
        RockScissorPaper.bestScore = RockScissorPaper.BestScore();
        if (isFileExist(Player1History) == true || isFileExist(Player2History) == true)
        {
            if (result == RockScissorPaper.PLAYER_1_WIN)
            {
                content = "Player 1 best score: " + RockScissorPaper.bestScore;
                File.WriteAllText(Player1History, content);
                Console.WriteLine("History save success !");
            }
            if ((result == RockScissorPaper.PLAYER_2_WIN))
            {
                content = "Player 2 best score: " + RockScissorPaper.bestScore;
                File.WriteAllText(Player2History, content);
                Console.WriteLine("History save success !");
            }
        }
        else
        {
            throw new Exception("Saving fail !");
        }
    }
    public void ReadPlayer1History()
    {
        string filePath = Player1History;

        if (isFileExist(filePath) == true)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }
        }
        else
        {
            throw new Exception("Can not read Player 1 History !");
        }
    }
    public void ReadPlayer2History()
    {
        string filePath = Player2History;

        if (isFileExist(filePath) == true)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }
        }
        else
        {
            throw new Exception("Can not read Player 2 History !");

        }
    }
    private bool isFileExist(string filePath)
    {
        if (File.Exists(filePath))
        {
            return true;
        }
        else
        {
            throw new Exception("File does not exist !");
        }
    }
}