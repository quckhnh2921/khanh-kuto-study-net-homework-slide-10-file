﻿Console.OutputEncoding = System.Text.Encoding.Default;
RockScissorPaper game = new RockScissorPaper();
WriteAndReadFile writeAndReadFile = new WriteAndReadFile();
int choice = 0;
int player1Choose;
int player2Choose;

do
{
    try
    {
        game.MainMenu();
            choice = int.Parse(Console.ReadLine());
            if(choice <1 || choice > 3)
            {
                throw new Exception("Choice is out of range");
            }
        switch (choice)
        {
            case 1:
                game.GameMenu();
                try
                {
                    Console.Write("Enter player 1 choose: ");
                    player1Choose = int.Parse(Console.ReadLine());
                    Console.Write("Enter player 2 choose: ");
                    player2Choose = int.Parse(Console.ReadLine());
                    var result = game.GameRule(player1Choose, player2Choose);
                    Console.WriteLine(game.GameResult(result));
                    writeAndReadFile.WriteGameHistory(result);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                break;
            case 2:
                writeAndReadFile.HistoryMenu();
                choice = int.Parse(Console.ReadLine());
                if (choice == 1)
                {
                    writeAndReadFile.ReadPlayer1History();
                }
                if (choice == 2)
                {
                    writeAndReadFile.ReadPlayer2History();
                }
                if (choice < 1 || choice > 2)
                {
                    throw new Exception("Choice is out of range !");
                }
                break;
            default:
                Console.WriteLine("BYE BYE !");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice < 3);

